name := "GuardrailTest"

version := "0.1"

scalaVersion := "2.13.1"

guardrailTasks in Compile := List(
  ScalaClient(file("api.yaml"))
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http"       % "10.1.11",
  "com.typesafe.akka" %% "akka-stream"     % "2.5.26",
  "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
  "io.circe"          %% "circe-core"      % "0.12.3",
  "io.circe"          %% "circe-generic"   % "0.12.3",
  "io.circe"          %% "circe-parser"    % "0.12.3"
)